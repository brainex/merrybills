<?php

namespace Brainex;

use Brainex\Exceptions\RequestException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Throwable;

class MerryBills
{
    public const ELECTRICITY_TYPE_PREPAID = 'PREPAID';
    public const ELECTRITICY_TYPE_POSTPAID = 'POSTPAID';

    private $base_url = 'https://merrybills.com/api';

    /** @var string */
    private $token;

    /** @var  string */
    private $pin;

    /** @var Client */
    private $client;

    /** @var int */
    private $timeout;


    /**
     * Class constrictor
     *
     * @param string $token
     */
    public function __construct(string $token, string $pin, int $timeout = 50)
    {
        $this->token = $token;
        $this->pin = $pin;
        $this->client = new Client();
        $this->timeout = $timeout;
    }

    /**
     * Get user account
     *
     * @return object
     */
    public function getAccount()
    {
        return $this->request('GET', '/me')->data->user;
    }

    /**
     * Get airtime
     *
     * @return object
     */
    public function getAirtime()
    {
        return $this->request('GET', '/airtime')->data;
    }

    /**
     * Purchase Airtime
     *
     * @param string $phone
     * @param string $product_id
     * @param string $amount
     * @param string|null $request_id
     * @return object
     */
    public function purchaseAirtime(string $phone, string $product_id, string $amount, ?string $request_id = null)
    {
        return $this->request('POST', '/airtime', [
            'phone' => $phone,
            'product_id' => $product_id,
            'amount' => $amount,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * Get data bundle
     *
     * @return object
     */
    public function getDataBundle()
    {
        return $this->request('GET', '/data')->data;
    }

    /**
     * Purchase data bundle
     *
     * @param string $phone
     * @param string $product_id
     * @param string $variation
     * @param string|null $request_id
     * @return object
     */
    public function purchaseDataBundle(string $phone, string $product_id, string $variation, ?string $request_id = null)
    {
        return $this->request('POST', '/data', [
            'phone' => $phone,
            'product_id' => $product_id,
            'variation' => $variation,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * Get sme data
     *
     * @return object
     */
    public function getDataSme()
    {
        return $this->request('GET', '/data/sme')->data;
    }

    /**
     * Purchase sme data
     *
     * @param string $phone
     * @param string $product_id
     * @param string $variation
     * @param string|null $request_id
     * @return object
     */
    public function purchaseDataSme(string $phone, string $product_id, string $variation, ?string $request_id = null)
    {
        return $this->request('POST', '/data/sme', [
            'phone' => $phone,
            'product_id' => $product_id,
            'variation' => $variation,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * Get cable
     *
     * @return object
     */
    public function getCable()
    {
        return $this->request('GET', '/cable')->data;
    }

    /**
     * Purchase cable
     *
     * @param string $iuc_number
     * @param string $product_id
     * @param string $variation
     * @param string|null $request_id
     * @return object
     */
    public function purchaseCable(string $iuc_number, string $product_id, string $variation, ?string $request_id = null)
    {
        return $this->request('POST', '/cable', [
            'iuc_number' => $iuc_number,
            'product_id' => $product_id,
            'variation' => $variation,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * Get electricity
     *
     * @return object
     */
    public function getElectricity()
    {
        return $this->request('GET', '/electricity')->data;
    }

    /**
     * Purchase electricity
     *
     * @param string $meter
     * @param string $product_id
     * @param float $amount
     * @param mixed $type
     * @param string|null $request_id
     * @return object
     */
    public function purchaseElectricity(string $meter, string $product_id, float $amount, $type, ?string $request_id = null)
    {
        return $this->request('POST', '/electricity', [
            'meter_number' => $meter,
            'product_id' => $product_id,
            'amount' => $amount,
            'type' => $type,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * Get Internet
     *
     * @return object
     */
    public function getInternet()
    {
        return $this->request('GET', '/internet')->data;
    }

    /**
     * Purchase internet
     *
     * @param string $smart_number
     * @param string $product_id
     * @param string $variation
     * @param string|null $request_id
     * @return object
     */
    public function purchaseInternet(string $smart_number, string $product_id, string $variation, ?string $request_id = null)
    {
        return $this->request('POST', '/internet', [
            'smart_number' => $smart_number,
            'product_id' => $product_id,
            'variation' => $variation,
            'pin' => $this->pin,
            'request_id' => $request_id
        ]);
    }

    /**
     * V
     *
     * @param string $iuc_number
     * @param string $product_id
     * @return object
     */
    public function verifyCable(string $iuc_number, string $product_id)
    {
        return $this->request('POST', '/verify-cable', [
            'iuc_number' => $iuc_number,
            'product_id' => $product_id
        ])->data;
    }

    /**
     * Verify electricity
     *
     * @param string $meter
     * @param string $product_id
     * @param string $type PREPAID | POSTPAID
     * @return object
     */
    public function verifyElectricity(string $meter, string $product_id, string $type)
    {
        return $this->request('POST', '/verify-meter', [
            'meter_number' => $meter,
            'product_id' => $product_id,
            'type' => $type
        ])->data;
    }

    /**
     * Make request
     *
     * @param string $method
     * @param string $endpoint
     * @param array|null $params
     * @return mixed
     */
    private function request(string $method, string $endpoint, ?array $params = null)
    {
        try {
            
            $response = $this->client->request($method, $this->base_url .  $endpoint, [
                RequestOptions::TIMEOUT => $this->timeout,
                RequestOptions::CONNECT_TIMEOUT => $this->timeout,
                RequestOptions::JSON => $params,
                RequestOptions::HEADERS => array(
                    'Authorization' => 'Bearer ' . $this->token,
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                )
            ]);

        } catch (Exception $e) {
            throw new RequestException($e);
        }

        $data = $response->getBody()->getContents();

        if(!$data) {
            throw new RequestException('No response');
        }

        $res = json_decode($data);

        if($res->status === 'error') {
            throw new RequestException($res->msg);
        }

        return $res;
    }
}